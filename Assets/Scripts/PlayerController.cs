﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{

    public float speedMultiplier = 0.5f;
    public float speedAcceleratorDelta = 0.1f;

    private float moveVector = 0.0f;
    private float moveAccelerator = 0.0f;

    private float actualSpeed;

    private TextController debugText;

    private void Awake()
    {
        debugText = GameObject.FindGameObjectWithTag("debugText").GetComponent<TextController>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((moveVector = Input.GetAxis("Horizontal")) != 0.0f)
        {
            moveAccelerator += speedAcceleratorDelta * Time.deltaTime;
            actualSpeed = moveVector * speedMultiplier + moveAccelerator * Mathf.Sign(moveVector);
            debugText.setText(string.Format("{0:f}", actualSpeed));
            transform.position = new Vector3(transform.position.x + actualSpeed, transform.position.y);
        }
        else if (moveAccelerator != 0.0f)
        {
            moveAccelerator = 0.0f;
            debugText.setText("0");
        }
    }
}
