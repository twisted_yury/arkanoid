﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


    public class TextController: MonoBehaviour {

    Text debugText;

    private void Awake()
    {
        debugText = GetComponent<Text>();
    }

    public void setText(string newText)
        {
            debugText.text = newText;
        }
    }


